package com.example.demo.entity

import com.example.demo.security.entity.JwtUser
import javax.persistence.*

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
abstract class UserAccount (
    open var email: String? = null,
    open var password: String? = null,
    open var firstname: String? = null,
    open var lastname: String? = null,
    open var imageUrl: String? = null,
    open var userStatus: UserStatus? = null
) {
    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToOne
    var jwtUser:JwtUser? = null
}