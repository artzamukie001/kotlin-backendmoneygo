package com.example.demo.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToMany

@Entity
data class User (
        override var email: String? = null,
        override var password: String? = null,
        override var firstname: String? = null,
        override var lastname: String? = null,
        override var imageUrl: String? = null,
        override var userStatus: UserStatus? = UserStatus.PENDING,
        var bankBalance: Double? = null
) : UserAccount(
        firstname,
        lastname,
        email,
        password,
        imageUrl,
        userStatus
)
{
    @OneToMany
    var wallets = mutableListOf<Wallet>()
}