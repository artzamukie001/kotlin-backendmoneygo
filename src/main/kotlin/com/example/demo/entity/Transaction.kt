package com.example.demo.entity

import java.time.LocalDate
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Transaction(
        var money: Double? = null,
        var date: LocalDate? = null,
        var Description: String? = null,
        var transactionStatus:TransactionStatus? = null,
        var financialType: FinancialType? = null,
        var isDeleted: Boolean = false
){
    @Id
    @GeneratedValue
    var id: Long? = null

}