package com.example.demo.entity.dto

data class UserAccountDto(
        var firstname: String? = null,
        var lastname: String? = null,
        var email: String? = null)
