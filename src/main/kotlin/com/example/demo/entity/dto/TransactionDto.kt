package com.example.demo.entity.dto

import com.example.demo.entity.FinancialType
import com.example.demo.entity.TransactionStatus
import java.time.LocalDate

data class TransactionDto(var money: Double? = null,
                          var date: LocalDate? = null,
                          var Description: String? = null,
                          var transactionStatus: TransactionStatus? = null,
                          var financialType: FinancialType? = null,
                          var bankBalance: Double? = null,
                          var id: Long? = null,
                          var isDeleted: Boolean? = false)