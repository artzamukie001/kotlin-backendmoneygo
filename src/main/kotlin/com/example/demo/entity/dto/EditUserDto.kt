package com.example.demo.entity.dto

data class EditUserDto(
        var email: String? = null,
        var password: String? = null,
        var firstname: String? = null,
        var lastname: String? = null,
        var imageUrl: String? = null
)