package com.example.demo.entity.dto

data class WalletDto(var name:String? = null,
                     var money:Double? = null,
                     var isDeleted: Boolean? = false,
                     var id:Long? = null,
                     var transactions: List<TransactionDto>? = emptyList())