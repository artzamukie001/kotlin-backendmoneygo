package com.example.demo.entity.dto

import com.example.demo.entity.UserStatus

data class UserDto(var email:String? = null,
                   var password:String? = null,
                   var firstname:String? = null,
                   var lastname:String? = null,
                   var imageUrl:String? = null,
                   var userStatus: UserStatus? = null,
                   var wallets:List<WalletDto>? = emptyList(),
                   var id:Long? = null,
                   var authorities:List<AuthorityDto> = mutableListOf())