package com.example.demo.entity.dto

data class EditWalletDto(var name: String? = null,
                         var id: Long? = null)
