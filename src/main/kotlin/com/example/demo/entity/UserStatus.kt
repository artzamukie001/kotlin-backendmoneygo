package com.example.demo.entity

enum class UserStatus{
    ACTIVE,DELETED,PENDING
}