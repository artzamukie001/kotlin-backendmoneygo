package com.example.demo.entity

enum class TransactionStatus {
    REVENUE, EXPENSE
}