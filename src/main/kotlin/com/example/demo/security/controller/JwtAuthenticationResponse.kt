package com.example.demo.security.controller

data class JwtAuthenticationResponse(
        var token: String? = null
)