package com.example.demo.security.repository

import com.example.demo.security.entity.JwtUser
import org.springframework.data.repository.CrudRepository

interface JwtUserRepository: CrudRepository<JwtUser, Long> {
    fun findByUsername(username: String): JwtUser
}
