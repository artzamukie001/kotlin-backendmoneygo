package com.example.demo.service

import com.example.demo.dao.UserDao
import com.example.demo.entity.User
import com.example.demo.entity.dto.EditUserDto
import com.example.demo.entity.dto.UserDto
import com.example.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserServiceImpl:UserService{
    override fun saveEditUser(userId: Long, editUserDto: EditUserDto): User {
        val user = userDao.findbyId(userId)
        user.firstname = editUserDto.firstname
        user.lastname = editUserDto.lastname
        user.password = editUserDto.password
        user.imageUrl = editUserDto.imageUrl
        return userDao.save(user)
    }

    override fun getUserByUserId(id: Long): User? {
        return userDao.getUserById(id)
    }

    override fun save(user:UserDto):User{
        val user = MapperUtil.INSTANCE.mapUser(user)
        return userDao.save(user)
    }

    @Autowired
    lateinit var userDao:UserDao
    override fun getUsers(): List<User> {
        return userDao.getUsers()
    }
}