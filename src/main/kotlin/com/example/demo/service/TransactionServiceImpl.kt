package com.example.demo.service

import com.example.demo.dao.TransactionDao
import com.example.demo.dao.WalletDao
import com.example.demo.entity.Transaction
import com.example.demo.entity.TransactionStatus
import com.example.demo.entity.dto.TransactionDto
import com.example.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.lang.RuntimeException
import javax.transaction.Transactional

@Service
class TransactionServiceImpl:TransactionService{
    @Transactional
    override fun saveTransaction(walletId: Long, trans: Transaction): Transaction {
        val wallet = walletDao.getWalletById(walletId)
//            val newTransaction = transactionDao.save(trans)
//            wallet.transactions?.add(newTransaction)
//            println(wallet.toString())
//            return newTransaction
//        }else{
//            throw RuntimeException("not found Wallet id")
//        }
            if (wallet != null) {
                val wallet = walletDao.findById(walletId)
                val trans = transactionDao.save(trans)
                wallet?.transactions?.add(trans)
                if (trans.transactionStatus == TransactionStatus.REVENUE) {
                    wallet.money = wallet.money!!.plus(trans.money!!)
                } else {
                    wallet.money = wallet.money!!.minus(trans.money!!)
                }
                return trans
            }else{
            throw RuntimeException("not found Wallet id")
        }
        }

    @Transactional
    override fun remove(id:Long): Transaction?{
        val trans = transactionDao.findById(id)
        trans?.isDeleted = true
        return trans
    }

    override fun save(trans:TransactionDto):Transaction{
        val trans = MapperUtil.INSTANCE.mapTransaction(trans)
        return transactionDao.save(trans)
    }

    @Autowired
    lateinit var transactionDao: TransactionDao

    override fun getTransactions(): List<Transaction> {
        return transactionDao.getTransactions()
    }

    @Autowired
    lateinit var walletDao:WalletDao
}