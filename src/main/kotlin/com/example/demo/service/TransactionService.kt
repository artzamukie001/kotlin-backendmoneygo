package com.example.demo.service

import com.example.demo.entity.Transaction
import com.example.demo.entity.dto.TransactionDto

interface TransactionService{
    fun getTransactions():List<Transaction>
    fun save(trans: TransactionDto): Transaction
    fun remove(id: Long): Transaction?
    fun saveTransaction(walletId: Long, trans: Transaction): Transaction
}