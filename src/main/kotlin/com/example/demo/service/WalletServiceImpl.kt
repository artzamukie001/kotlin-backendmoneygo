package com.example.demo.service

import com.example.demo.dao.UserDao
import com.example.demo.dao.WalletDao
import com.example.demo.entity.Wallet
import com.example.demo.entity.dto.EditWalletDto
import com.example.demo.entity.dto.WalletDto
import com.example.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.lang.RuntimeException
import javax.transaction.Transactional

@Service
class WalletServiceImpl:WalletService{
    override fun saveNewWallet(walletId: Long, editWalletDto: EditWalletDto): Wallet {
        val wallet = walletDao.findById(walletId)
        wallet.name = editWalletDto.name
        return walletDao.save(wallet)
    }


    @Transactional
    override fun saveWallet(userId: Long, wallet: Wallet): Wallet {
        val user = userDao.getUserById(userId)
        if (user != null){
            val newWallet = walletDao.save(wallet)
            user?.wallets?.add(newWallet)
            return newWallet
        } else {
            throw RuntimeException("Not found WalletId")
        }
    }

    @Transactional
    override fun remove(id:Long): Wallet? {
        val wallet = walletDao.findById(id)
        wallet?.isDeleted = true
        return wallet
    }

    override fun save(wallet: WalletDto): Wallet {
        val wallet = MapperUtil.INSTANCE.mapWalletDto(wallet)
        return walletDao.save(wallet)
    }

    @Autowired
    lateinit var walletDao: WalletDao
    override fun getWallets(): List<Wallet> {
        return walletDao.getWallets()
    }

    @Autowired
    lateinit var userDao: UserDao
}