package com.example.demo.service

import com.example.demo.entity.User
import com.example.demo.entity.dto.EditUserDto
import com.example.demo.entity.dto.UserDto

interface UserService {
    fun getUsers():List<User>
    fun save(user: UserDto): User
    fun getUserByUserId(id: Long): User?
    fun saveEditUser(userId: Long, editUserDto: EditUserDto): User
}