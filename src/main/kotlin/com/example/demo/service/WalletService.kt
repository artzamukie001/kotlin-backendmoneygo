package com.example.demo.service

import com.example.demo.entity.Wallet
import com.example.demo.entity.dto.EditWalletDto
import com.example.demo.entity.dto.WalletDto
import javax.transaction.Transactional

interface WalletService{

    fun getWallets():List<Wallet>
    fun save(wallet: WalletDto): Wallet
    @Transactional
    fun remove(id: Long): Wallet?

    fun saveWallet(userId: Long, wallet: Wallet): Wallet
    fun saveNewWallet(walletId: Long, editWalletDto: EditWalletDto): Wallet
}