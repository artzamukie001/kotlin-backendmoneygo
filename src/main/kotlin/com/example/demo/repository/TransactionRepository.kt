package com.example.demo.repository

import com.example.demo.entity.Transaction
import org.springframework.data.repository.CrudRepository

interface TransactionRepository:CrudRepository<Transaction,Long>{
    fun findByIsDeletedIsFalse():List<Transaction>
}