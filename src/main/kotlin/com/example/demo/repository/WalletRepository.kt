package com.example.demo.repository

import com.example.demo.entity.Wallet
import org.springframework.data.repository.CrudRepository

interface WalletRepository:CrudRepository<Wallet,Long>{
    fun findByIsDeletedIsFalse():List<Wallet>
}