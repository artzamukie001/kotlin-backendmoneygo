package com.example.demo.controller

import com.example.demo.entity.dto.EditUserDto
import com.example.demo.entity.dto.UserDto
import com.example.demo.service.UserService
import com.example.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
class UserController{

    @Autowired
    lateinit var userService: UserService

    @GetMapping("/user")
    fun getAllUser():ResponseEntity<Any>{
        val output = userService.getUsers()
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUser(output))
    }

    @GetMapping("/users/userId/{id}")
    fun getUserByUserId(@PathVariable("id")id:Long):ResponseEntity<Any>{
        val output = userService.getUserByUserId(id)
        output?.let { return ResponseEntity.ok(output) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found user id")
    }

    @PostMapping("/users")
    fun addUser(@RequestBody user: UserDto):ResponseEntity<Any>{
        return ResponseEntity.ok(MapperUtil.INSTANCE.mapUser(userService.save(user)))
    }
    @PutMapping("/user/EditUser/{userId}")
    fun editUser(@PathVariable("userId")userId:Long,
                 @RequestBody editUserDto: EditUserDto):ResponseEntity<Any>{
        try {
            val output = userService.saveEditUser(userId,editUserDto)
            return ResponseEntity.ok(output)
        }catch (e: RuntimeException) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found userId")
        }

    }


}