package com.example.demo.controller

import com.example.demo.entity.dto.EditWalletDto
import com.example.demo.entity.dto.WalletDto
import com.example.demo.service.WalletService
import com.example.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.lang.RuntimeException

@RestController
class WalletController {
    @DeleteMapping("/wallets/{id}")
    fun deleteWallet(@PathVariable("id") id: Long): ResponseEntity<Any> {
        val wallet = walletService.remove(id)
        val outputWallet = MapperUtil.INSTANCE.mapWalletDto(wallet)
        outputWallet?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the wallet id is not found")
    }

//    @PostMapping("/wallets")
//    fun addWallet(@RequestBody wallet: WalletDto): ResponseEntity<Any> {
//        return ResponseEntity.ok(MapperUtil.INSTANCE.mapWallet(walletService.save(wallet)))
//    }
    @PostMapping("/wallets/userId/{userId}")
    fun addWallet(@PathVariable("userId")userId:Long,
                  @RequestBody wallet: WalletDto):ResponseEntity<Any>{
    try {
        val output = walletService.saveWallet(userId,MapperUtil.INSTANCE.mapWalletDto(wallet))
        val outputDto = MapperUtil.INSTANCE.mapWalletDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
    } catch (e: RuntimeException){
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found userId")
    }
}

    @Autowired
    lateinit var walletService: WalletService

    @GetMapping("/wallets")
    fun getAllWallet(): ResponseEntity<Any> {
        return ResponseEntity.ok(walletService.getWallets())
    }

    @PutMapping("/wallets/EditWallets/{EditWallet}")
    fun editWallet(@PathVariable("EditWallet")walletId:Long,
                   @RequestBody editWalletDto: EditWalletDto):ResponseEntity<Any>{
        try {
            val output = walletService.saveNewWallet(walletId,editWalletDto)
            return ResponseEntity.ok(output)
        }catch (e:RuntimeException){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found wallet id")
        }
    }
}