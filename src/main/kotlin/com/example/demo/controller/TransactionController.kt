package com.example.demo.controller

import com.example.demo.entity.Transaction
import com.example.demo.entity.dto.EditUserDto
import com.example.demo.entity.dto.TransactionDto
import com.example.demo.service.TransactionService
import com.example.demo.util.MapperUtil
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.lang.RuntimeException

@RestController
class TransactionController{
    @DeleteMapping("/transactions/{id}")
    fun deleteTransaction(@PathVariable("id")id:Long):ResponseEntity<Any>{
        val trans = transactionService.remove(id)
        val outputTrans = MapperUtil.INSTANCE.mapTransactionDto(trans)
        outputTrans?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("the transaction id is not found")
    }

//    @PostMapping("/transactions")
//    fun addTransaction(@RequestBody trans: TransactionDto):ResponseEntity<Any>{
//        return ResponseEntity.ok(MapperUtil.INSTANCE.mapTransaction(transactionService.save(trans)))
//    }
    @PostMapping("/transaction/walletId/{walletId}")
    fun addTransaction(@PathVariable("walletId")walletId:Long,
                       @RequestBody trans: Transaction):ResponseEntity<Any>{
    try {
        val output = transactionService.saveTransaction(walletId,trans)
        val outputDto = MapperUtil.INSTANCE.mapTransactionDto(output)
        outputDto?.let { return ResponseEntity.ok(it) }
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).build()
    } catch (e: RuntimeException){
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("not found wallet id")
    }
}

    @Autowired
    lateinit var transactionService: TransactionService
    @GetMapping("/transaction")
    fun getAllTransaction():ResponseEntity<Any>{
        return ResponseEntity.ok(transactionService.getTransactions())
    }


}