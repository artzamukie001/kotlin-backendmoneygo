package com.example.demo.dao

import com.example.demo.entity.FinancialType
import com.example.demo.entity.Transaction
import com.example.demo.entity.TransactionStatus
import com.example.demo.repository.TransactionRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import java.time.LocalDate

@Profile("db")
@Repository
class TransactionDaoDBImpl:TransactionDao{
    override fun findById(id:Long):Transaction?{
        return transactionRepository.findById(id).orElse(null)
    }

    @Autowired
    lateinit var transactionRepository: TransactionRepository
    override fun save(trans: Transaction): Transaction {
        return transactionRepository.save(trans)
    }

    override fun getTransactions(): List<Transaction> {
//        return mutableListOf(Transaction(500.00, LocalDate.parse("2019-03-23"),"adfasdf",TransactionStatus.EXPENSE,FinancialType.FOOD))
    return transactionRepository.findByIsDeletedIsFalse()
   }
}