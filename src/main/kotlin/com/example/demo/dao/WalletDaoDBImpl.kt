package com.example.demo.dao

import com.example.demo.entity.Wallet
import com.example.demo.repository.WalletRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class WalletDaoDBImpl:WalletDao{

    override fun findById(id: Long): Wallet {
        return walletRepository.findById(id!!).orElse(null)
    }

    override fun getWalletById(id: Long): Wallet? {
        return walletRepository.findById(id).orElse(null)
    }


    @Autowired
    lateinit var walletRepository: WalletRepository
    override fun save(wallet: Wallet): Wallet {
        return walletRepository.save(wallet)
    }

    override fun getWallets(): List<Wallet> {
//        return mutableListOf(Wallet("wow",100.00),
//                Wallet("mom",500.00))
        return walletRepository.findByIsDeletedIsFalse()
    }
}