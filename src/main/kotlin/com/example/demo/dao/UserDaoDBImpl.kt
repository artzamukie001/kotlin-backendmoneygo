package com.example.demo.dao

import com.example.demo.entity.User
import com.example.demo.entity.UserStatus
import com.example.demo.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.data.jpa.domain.AbstractPersistable_.id
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class UserDaoDBImpl:UserDao {
    override fun findbyId(userId: Long): User {
        return userRepository.findById(userId).orElse(null)
    }

    override fun getUserById(id: Long): User? {
        return userRepository.findById(id).orElse(null)
    }

    @Autowired
    lateinit var userRepository: UserRepository
    override fun save(user: User): User {
        return userRepository.save(user)
    }

    override fun getUsers(): List<User> {
        return userRepository.findAll().filterIsInstance(User::class.java)
    }
}