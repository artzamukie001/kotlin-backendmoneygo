package com.example.demo.dao

import com.example.demo.entity.Transaction

interface TransactionDao{
    fun getTransactions():List<Transaction>
    fun save(trans: Transaction): Transaction
    fun findById(id: Long): Transaction?
}