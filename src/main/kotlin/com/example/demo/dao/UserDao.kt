package com.example.demo.dao

import com.example.demo.entity.User

interface UserDao {
    fun getUsers(): List<User>
    fun save(user: User): User
    fun getUserById(id: Long): User?
    fun findbyId(userId: Long): User

}