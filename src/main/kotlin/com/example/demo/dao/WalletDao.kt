package com.example.demo.dao

import com.example.demo.entity.Wallet

interface WalletDao{
    fun getWallets():List<Wallet>
    fun save(wallet: Wallet): Wallet
    fun findById(id: Long): Wallet
    fun getWalletById(id:Long): Wallet?

//    fun findById(id: Long?): Wallet
}