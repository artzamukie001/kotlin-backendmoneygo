package com.example.demo.config


import com.example.demo.entity.User
import com.example.demo.entity.UserStatus
import com.example.demo.entity.Wallet
import com.example.demo.repository.UserRepository
import com.example.demo.repository.WalletRepository
import com.example.demo.security.entity.Authority
import com.example.demo.security.entity.AuthorityName
import com.example.demo.security.entity.JwtUser
import com.example.demo.security.repository.AuthorityRepository
import com.example.demo.security.repository.JwtUserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
class ApplicationLoader: ApplicationRunner{
    @Autowired
    lateinit var authorityRepository: AuthorityRepository
    @Autowired
    lateinit var userRepository: UserRepository
    @Transactional
    fun loadUsernameAndPassword(){
        val auth1 = Authority(name = AuthorityName.ROLE_ADMIN)
        val auth2 = Authority(name = AuthorityName.ROLE_CUSTOMER)
        val auth3 = Authority(name = AuthorityName.ROLE_GENERAL)
        authorityRepository.save(auth1)
        authorityRepository.save(auth2)
        authorityRepository.save(auth3)
        val encoder = BCryptPasswordEncoder()
        val user1 = User("art@art.com","55555","artart","bodybody","adsfjadf",UserStatus.PENDING,5.00)
        val userJwt = JwtUser(
                username = "User",
                password = encoder.encode("password"),
                email = user1.email,
                enabled = true,
                firstname = user1.firstname,
                lastname = user1.lastname
        )
        userRepository.save(user1)
        jwtUserRepository.save(userJwt)
        user1.jwtUser = userJwt
        userJwt.user = user1
        userJwt.authorities.add(auth1)
        userJwt.authorities.add(auth2)
        userJwt.authorities.add(auth3)
    }



    @Autowired
    lateinit var walletRepository: WalletRepository

    @Autowired
    lateinit var jwtUserRepository: JwtUserRepository


    @Transactional
    override fun run(args: ApplicationArguments?) {
        var wallet1 = walletRepository.save(Wallet("Wow",100.00))
        var userr1 = userRepository.save(User("akjdf@a.com","12356","artttra","mario","adfjksadf",UserStatus.PENDING,10.00))

        loadUsernameAndPassword()
    }



}