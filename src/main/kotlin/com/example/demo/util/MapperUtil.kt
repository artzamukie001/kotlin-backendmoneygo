package com.example.demo.util

import com.example.demo.entity.Transaction
import com.example.demo.entity.User
import com.example.demo.entity.Wallet
import com.example.demo.entity.dto.*
import com.example.demo.security.entity.Authority
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.Mapping
import org.mapstruct.Mappings
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "srping")
interface MapperUtil{
    @InheritInverseConfiguration
    fun mapUser(user: UserDto): User
    fun mapUser(user:User):UserDto

//    @InheritInverseConfiguration
//    fun mapWallet(wallet: WalletDto): Wallet
//    fun mapWallet(wallet: Wallet): WalletDto

    @InheritInverseConfiguration
    fun mapTransaction(trans: TransactionDto): Transaction
    fun mapTransaction(trans: Transaction): TransactionDto

//    fun mapWalletDto(wallet: Wallet?): Wallet
    fun mapTransactionDto(trans: Transaction?): Transaction
//    fun mapWalletDto(wallet: WalletDto): Wallet

    fun mapWalletDto(wallet: Wallet?): WalletDto?
    fun mapWalletDto(wallet: List<Wallet>): List<WalletDto>
    @InheritInverseConfiguration
    fun mapWalletDto(walletDto: WalletDto): Wallet

    fun mapUser(user: List<User>): List<UserDto>

    fun mapAuthority(authority: Authority): AuthorityDto
    fun mapAuthority(authority: List<Authority>): List<AuthorityDto>
    fun mapUserAccountDto(currentUser: User): UserAccountDto


    companion object {
        val INSTANCE = Mappers.getMapper(MapperUtil::class.java)
    }
}